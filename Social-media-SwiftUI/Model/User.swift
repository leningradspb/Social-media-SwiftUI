//
//  User.swift
//  Social-media-SwiftUI
//
//  Created by Eduard Kanevskii on 23.08.2023.
//

import Foundation
import FirebaseFirestoreSwift

struct User: Identifiable, Codable {
   @DocumentID var id: String?
    var userName: String
    var userBio: String
    var userBioLink: String
    var userID: String
    var userEmail: String
    var userProfileURL: URL
}
