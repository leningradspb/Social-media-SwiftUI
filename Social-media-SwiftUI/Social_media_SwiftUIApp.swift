//
//  Social_media_SwiftUIApp.swift
//  Social-media-SwiftUI
//
//  Created by Eduard Kanevskii on 22.08.2023.
//

import SwiftUI
import Firebase

@main
struct Social_media_SwiftUIApp: App {
//    @UIApplicationDelegateAdaptor(AppDelegate.self) var delegate
    init() {
        FirebaseApp.configure()
    }
    
    var body: some Scene {
        WindowGroup {
            ContentView()
//            LoginView(emailID: "", password: "")
//            RegisterView(userName: "", emailID: "", password: "", userBio: "", userBioLink: "")
        }
    }
}
