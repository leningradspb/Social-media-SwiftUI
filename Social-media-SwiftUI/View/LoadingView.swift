//
//  LoadingView.swift
//  Social-media-SwiftUI
//
//  Created by Eduard Kanevskii on 23.08.2023.
//

import SwiftUI

struct LoadingView: View {
    @Binding var isShow: Bool
    
    var body: some View {
        ZStack {
            if isShow {
                Group {
                    Rectangle()
                        .fill(.black.opacity(0.25))
                        .ignoresSafeArea()
                    
                    ProgressView().padding(15).background(.white, in: RoundedRectangle(cornerRadius: 10, style: .continuous))
                }
            }
        }
        .animation(.easeInOut(duration: 0.3), value: isShow)
    }
}
