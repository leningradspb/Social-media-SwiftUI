//
//  RegisterView.swift
//  Social-media-SwiftUI
//
//  Created by Eduard Kanevskii on 22.08.2023.
//

import SwiftUI
import PhotosUI
import Firebase
import FirebaseFirestore
import FirebaseStorage

struct RegisterView: View {
    @State var userName: String
    @State var emailID: String
    @State var password: String
    @State var userBio: String
    @State var userBioLink: String
    @State var userProfilePicData: Data?
    @State var showImagePicker: Bool = false
    @State var isLoading = false
    // Настройка обрезки для реального проекта https://www.youtube.com/watch?v=1Fz86eQjxus&list=PLVOa26uDzY2RTykeI-mVSTnEdiR4sKaed&index=18&t=37s&pp=gAQBiAQB
    @State var selectedPhotoItem: PhotosPickerItem?
    @State var showError = false
    @State var errorMessage = ""
    @AppStorage("isLoggedIn") var isLoggedIn = false
    @AppStorage("userID") var userID = ""
    
    @Environment(\.dismiss) var dismiss
    
    var body: some View {
        NavigationStack {
            ViewThatFits {
                ScrollView(.vertical, showsIndicators: false) {
                    VStack {
                        
                        if let userProfilePicData, let image = UIImage(data: userProfilePicData) {
                            Image(uiImage: image)
                                .resizable()
                                .aspectRatio(contentMode: .fill)
                                .frame(height: 100)
                                .clipShape(Circle())
                                .contentShape(Circle())
                                .onTapGesture {
                                    showImagePicker.toggle()
                                }
                        } else {
                            Image(systemName: "person.fill.viewfinder")
                                .renderingMode(.original)
                                .resizable()
                                .aspectRatio(contentMode: .fit)
                                .frame(height: 100)
                                .onTapGesture {
                                    showImagePicker.toggle()
                                }
                        }
                            
                        
                        TextField("User name", text: $userName)
                            .textContentType(.username)
                            .border(1, .gray.opacity(0.5))
                        
                        TextField("Email", text: $emailID)
                            .textContentType(.emailAddress)
                            .border(1, .gray.opacity(0.5))
                        
                        SecureField("Password", text: $password)
                            .textContentType(.password)
                            .border(1, .gray.opacity(0.5))
                        
                        TextField("About you", text: $userBio)
                            .frame(height: 100, alignment: .top)
                            .textContentType(.username)
                            .border(1, .gray.opacity(0.5))
                        
                        TextField("Link (Optional)", text: $userBioLink)
                            .textContentType(.emailAddress)
                            .border(1, .gray.opacity(0.5))
                        
                        Spacer().frame(height: 20)
                        
                        Button(action: registerUser) {
                            Text("Sign up")
                                .font(.title)
                                .foregroundColor(.white)
//                                .background(Color.black)
                                .hAlign(.center)
                                .fillView(.black)
                        }
                        .disableWithOpacity(emailID.isEmpty || password.isEmpty)
                        
                        Spacer()
                        
                        HStack {
                            Text("Already have an account?")
                                .foregroundColor(.gray)
                            
                            Button {
                                dismiss()
                            } label: {
                                Text("Login now")
                            }
                            .fontWeight(.bold)
                            .foregroundColor(.black)
                        }
                        
                    }
                    .padding()
                    .navigationTitle("Lets sign up you")
                    .toolbar {
                        Button("About") {
                            print("About tapped!")
                        }
                        
                        Button("Help") {
                            print("Help tapped!")
                        }
                    }
                }
            }
            .photosPicker(isPresented: $showImagePicker, selection: $selectedPhotoItem)
            .onChange(of: selectedPhotoItem) { newValue in
                if let newValue {
                    Task {
                        isLoading = true
                        do {
                            guard let imageData = try await newValue.loadTransferable(type: Data.self) else {
                                isLoading = false
                                return
                            }
                            // MAIN thread
                            await MainActor.run(body: {
                                isLoading = false
                                userProfilePicData = imageData
                            })
                        } catch {
                            isLoading = false
                            print("Failed photo")
                        }
                    }
                }
            }
            .alert(errorMessage, isPresented: $showError, actions: {})
            .overlay {
                LoadingView(isShow: $isLoading)
            } 
        }
    }
    
//    @ViewBuilder func HelperView() -> some View {
//        VStack {
//            TextField("User name", text: $userName)
//                .textContentType(.username)
//                .border(1, .gray.opacity(0.5))
//
//            TextField("Email", text: $emailID)
//                .textContentType(.emailAddress)
//                .border(1, .gray.opacity(0.5))
//
//            SecureField("Password", text: $password)
//                .textContentType(.password)
//                .border(1, .gray.opacity(0.5))
//
//            TextField("About you", text: $userBio)
//                .frame(height: 100, alignment: .top)
//                .textContentType(.username)
//                .border(1, .gray.opacity(0.5))
//
//            TextField("Link (Optional)", text: $userBioLink)
//                .textContentType(.emailAddress)
//                .border(1, .gray.opacity(0.5))
//
//            Spacer().frame(height: 20)
//
//            Button {
//                print("Button tapped")
//            } label: {
//                Text("Sign up")
//                    .font(.title)
//                    .foregroundColor(.white)
//                    .background(Color.black)
//                    .hAlign(.center)
//                    .fillView(.black)
//            }
//
//            Spacer()
//
//            HStack {
//                Text("Already have an account?")
//                    .foregroundColor(.gray)
//
//                Button {
//                    dismiss()
//                } label: {
//                    Text("Login now")
//                }
//                .fontWeight(.bold)
//                .foregroundColor(.black)
//            }
//
//        }
//        .padding()
//        .navigationTitle("Lets sign up you")
//        .toolbar {
//            Button("About") {
//                print("About tapped!")
//            }
//
//            Button("Help") {
//                print("Help tapped!")
//            }
//        }
//    }
    
    func registerUser() {
        closeKeyboard()
        isLoading = true
        Task {
            do {
                try await Auth.auth().createUser(withEmail: emailID, password: password)
                guard let userID = Auth.auth().currentUser?.uid else { return }
                guard let imageData = userProfilePicData else { return }
                let storageRef = Storage.storage().reference().child("Profile_images").child(userID)
                let _ = try await storageRef.putDataAsync(imageData)
                let downloadURL = try await storageRef.downloadURL()
                let user = User(userName: userName, userBio: userBio, userBioLink: userBioLink, userID: userID, userEmail: emailID, userProfileURL: downloadURL)
                let  _ = try Firestore.firestore().collection("Users").document(userID).setData(from: user) { error in
                    if error == nil {
                        print("Saved successfully")
                        isLoggedIn = true
                        isLoading = false
                        self.userID = userID
                    }
                }
            } catch {
                try await Auth.auth().currentUser?.delete()
                await setError(error)
            }
        }
    }
    
    func setError(_ error: Error) async {
        await MainActor.run(body: {
            errorMessage = error.localizedDescription
            isLoading = false
            showError.toggle()
        })
    }
}

struct RegisterView_Previews: PreviewProvider {
    static var previews: some View {
        RegisterView(userName: "", emailID: "", password: "", userBio: "", userBioLink: "")
    }
}
