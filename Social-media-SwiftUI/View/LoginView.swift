//
//  LoginView.swift
//  Social-media-SwiftUI
//
//  Created by Eduard Kanevskii on 22.08.2023.
//

import SwiftUI
import Firebase

struct LoginView: View {
    @State var emailID: String
    @State var password: String
    @State var createAccount = false
    @State var showError = false
    @State var errorMessage = ""
    @State var isLoading = false
    @AppStorage("isLoggedIn") var isLoggedIn = false
    @AppStorage("userID") var userID = ""
    @Environment(\.dismiss) var dismiss
    
    var body: some View {
        NavigationStack {
            VStack() {
                TextField("Email", text: $emailID)
                    .textContentType(.emailAddress)
                    .border(1, .gray.opacity(0.5))
                
                SecureField("Password", text: $password)
                    .textContentType(.password)
                    .border(1, .gray.opacity(0.5))
                
                Spacer().frame(height: 20)
                
                Button {
                    logInUser()
                } label: {
                    Text("Sign in")
                        .font(.title)
                        .foregroundColor(.white)
//                        .background(Color.black)
                        .hAlign(.center)
                        .fillView(.black)
                }
                
                Spacer()
                
                HStack {
                    Text("Don't have an account?")
                        .foregroundColor(.gray)
                    
                    Button {
                        createAccount.toggle()
                    } label: {
                        Text("Register now")
                    }
                    .fontWeight(.bold)
                    .foregroundColor(.black)
                }

            }
            .padding()
                .navigationTitle("Lets sign you")
                .toolbar {
                    Button("About") {
                        print("About tapped!")
                    }
                    
                    Button("Help") {
                        print("Help tapped!")
                    }
                }
                .overlay {
                    LoadingView(isShow: $isLoading)
                }
        }
        .fullScreenCover(isPresented: $createAccount) {
            RegisterView(userName: "", emailID: "", password: "", userBio: "", userBioLink: "")
        }
        .alert(errorMessage, isPresented: $showError, actions: {})
    }
    
    func logInUser() {
        closeKeyboard()
        isLoading = true
        Task {
            do {
                try await Auth.auth().signIn(withEmail: emailID, password: password)
                print("User found")
                isLoggedIn = true
               
                if let userID = Auth.auth().currentUser?.uid {
                    self.userID = userID
                }
                isLoading = false
                dismiss()
            } catch {
                await setError(error)
            }
        }
    }
    
    func setError(_ error: Error) async {
        await MainActor.run(body: {
            errorMessage = error.localizedDescription
            isLoading = false
            showError.toggle()
        })
    }
}

struct LoginView_Previews: PreviewProvider {
    static var previews: some View {
        LoginView(emailID: "", password: "")
    }
}


extension View {
    func border(_ width: CGFloat, _ color: Color) -> some View {
        self.padding(.horizontal, 15)
            .padding(.vertical, 10)
            .background {
                RoundedRectangle(cornerRadius: 5, style: .continuous)
                    .stroke(color, lineWidth: width)
            }
    }
    
    func hAlign(_ alignment: Alignment) -> some View {
        self.frame(maxWidth: .infinity, alignment: alignment)
    }
    
    func vAlign(_ alignment: Alignment) -> some View {
        self.frame(maxHeight: .infinity, alignment: alignment)
    }
    
    func fillView(_ color: Color) -> some View {
        self.padding(.horizontal, 15)
            .padding(.vertical, 10)
            .background {
                RoundedRectangle(cornerRadius: 5, style: .continuous)
                    .fill(color)
            }
    }
    
    func disableWithOpacity(_ condition: Bool) -> some View {
        self.disabled(condition).opacity(condition ? 0.6 : 1)
    }
    
    func closeKeyboard() {
        UIApplication.shared.sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)
    }
}
