//
//  ContentView.swift
//  Social-media-SwiftUI
//
//  Created by Eduard Kanevskii on 22.08.2023.
//

import SwiftUI

struct ContentView: View {
    @AppStorage("isLoggedIn") var isLoggedIn = false
    
    var body: some View {
        if isLoggedIn {
            Text("Logged in")
        } else {
            LoginView(emailID: "", password: "")
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
